# docker-buildx-qemu-ci
`docker buildx` images inside gitlab CI with qemu.

Fork of https://gitlab.com/com.hillnz.docker/docker-buildx-qemu,
which includes fixes for issues that i stumbled upon when building a rust project.


# Example usage:

``` yaml
variables:
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375/

services:
  - docker:dind

.build-arch-template: &build-template
  image: maksrawski/docker-buildx-qemu-ci

  before_script:
    - bash /setup.sh

  script:
    - docker buildx build --platform linux/$CI_JOB_NAME
      -t img-name -o type=docker,dest=- . > img.tar

  artifacts:
    paths:
      - img.tar

amd64:
    stage: build
    <<: *build-template

arm64:
    stage: build
    <<: *build-template
```

`variables`, `services`, `image` and `before_script` are the only necessary keys. 
Rest is included just to show how one could use this image 
to easily compile into multiple platforms simultaneously.

## Notes
When building for different platform than native (most likely amd64)
It's necessary to include [this fix](https://github.com/microsoft/WSL/issues/4760#issuecomment-642715044)
to not segfault when libc-bin is processed during any `apt-get install`.


I only know of this issue happening when building debian based images. And as I'm not sure 
whether it appears on other distributions, it's left to the user to either
hard code that fix into the `Dockerfile` being built or to put this monstrosity below into `.gitlab-ci.yml`
to automatically place the fix inside the `Dockerfile` before the very first `apt-get install`. 

```yaml
- sed -i -E 's?^.*apt-get update.*$?\0\n
    RUN mv /var/lib/dpkg/info/libc-bin.* /tmp/ \&\&
    dpkg --remove --force-remove-reinstreq --force-remove-essential --force-depends libc-bin \&\&
    dpkg --purge libc-bin \&\&
    apt-get install -y libc-bin \&\&
    mv /tmp/libc-bin.* /var/lib/dpkg/info/?' Dockerfile
```

Again for a different distro you may need to modify it or maybe even skip it completely.
