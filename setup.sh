[ -e /var/run/docker.sock ] || (echo "Can't access docker daemon!"; exit 1)

# qemu-user-static seems to not be enough
# https://askubuntu.com/a/1398147
docker run --privileged --rm tonistiigi/binfmt --uninstall qemu-*
docker run --privileged --rm tonistiigi/binfmt --install all

# create buildx driver
docker buildx create --driver docker-container --use
docker buildx inspect --bootstrap

# enable direct execution of files from different architectures
update-binfmts --enable
