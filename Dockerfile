FROM debian

RUN apt-get update && apt-get install -y docker.io apt-transport-https ca-certificates binfmt-support qemu-user-static

# install buildx; https://github.com/docker/buildx#dockerfile
COPY --from=docker/buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx
COPY setup.sh /setup.sh
